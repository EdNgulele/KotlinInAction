fun main(args: Array<String>){
    print(max(3,4))
}


/**
 * Expression bodies
 */
//fun max(a: Int, b: Int):Int = if(a>b) a else b //With a Return-type declaration
fun max(a: Int, b: Int)  = if(a>b) a else b //without the return declaration

