package ClassesAndProperties;

public class PersonJava {


    public static void main(String args[]) {


        //From Kotlin class to Java class
        PersonKotlin personKotlin = new PersonKotlin("Batman", false);

        System.out.println(personKotlin.getName());
        System.out.println(personKotlin.getIsMarried());

    }


    private final String name;
    private boolean isMarried;

    public PersonJava(String name, boolean isMarried) {
        this.name = name;
        this.isMarried = isMarried;
    }

    public String getName() {
        return name;
    }


    public boolean isMarried() {
        return isMarried;
    }

    public void setMarried(boolean married) {
        isMarried = married;
    }
}




